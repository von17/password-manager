package com.app.root.app;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.app.root.app.util.SystemUiHider;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

import butterknife.ButterKnife;
import butterknife.InjectView;


/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 *
 * @see SystemUiHider
 */
public class MainActivity extends Activity {
    /**
     * Whether or not the system UI should be auto-hidden after
     * {@link #AUTO_HIDE_DELAY_MILLIS} milliseconds.
     */
    private static final boolean AUTO_HIDE = true;

    /**
     * If {@link #AUTO_HIDE} is set, the number of milliseconds to wait after
     * user interaction before hiding the system UI.
     */
    private static final int AUTO_HIDE_DELAY_MILLIS = 3000;

    /**
     * If set, will toggle the system UI visibility upon interaction. Otherwise,
     * will show the system UI visibility upon interaction.
     */
    private static final boolean TOGGLE_ON_CLICK = true;

    /**
     * The flags to pass to {@link SystemUiHider#getInstance}.
     */
    private static final int HIDER_FLAGS = SystemUiHider.FLAG_HIDE_NAVIGATION;

    /**
     * The instance of the {@link SystemUiHider} for this activity.
     */
    private SystemUiHider mSystemUiHider;

    private static final String DIR = "com.app.root.app";
    public static final File DIR_EXTERNAL = new File(
            Environment.getExternalStorageDirectory(),
            DIR
    );
    public static File DIR_INTERNAL;
    private static final String FILE = "cnf";
    private static final String ENCRYPT_ALGO = "SHA-512";
    private static final int BYTE_LENGTH = 64;

    @InjectView(R.id.fullscreen_content_controls)
    LinearLayout controlsView;

    @InjectView(R.id.fullscreen_content)
    FrameLayout contentView;
    @InjectView(R.id.first_edit_text)
    EditText firstText;
    @InjectView(R.id.second_edit_text)
    EditText secondText;
    @InjectView(R.id.dummy_button)
    Button dummyButton;

    private MessageDigest md;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.inject(this);

        // Set up an instance of SystemUiHider to control the system UI for
        // this activity.
        mSystemUiHider = SystemUiHider.getInstance(this, contentView, HIDER_FLAGS);
        mSystemUiHider.setup();
        mSystemUiHider
                .setOnVisibilityChangeListener(new SystemUiHider.OnVisibilityChangeListener() {
                    // Cached values.
                    int mControlsHeight;
                    int mShortAnimTime;

                    @Override
                    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
                    public void onVisibilityChange(boolean visible) {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
                            // If the ViewPropertyAnimator API is available
                            // (Honeycomb MR2 and later), use it to animate the
                            // in-layout UI controls at the bottom of the
                            // screen.
                            if (mControlsHeight == 0) {
                                mControlsHeight = controlsView.getHeight();
                            }
                            if (mShortAnimTime == 0) {
                                mShortAnimTime = getResources().getInteger(
                                        android.R.integer.config_shortAnimTime);
                            }
                            controlsView.animate()
                                    .translationY(visible ? 0 : mControlsHeight)
                                    .setDuration(mShortAnimTime);
                        } else {
                            // If the ViewPropertyAnimator APIs aren't
                            // available, simply show or hide the in-layout UI
                            // controls.
                            controlsView.setVisibility(visible ? View.VISIBLE : View.GONE);
                        }

                        if (visible && AUTO_HIDE) {
                            // Schedule a hide().
                            delayedHide(AUTO_HIDE_DELAY_MILLIS);
                        }
                    }
                });

        // Set up the user interaction to manually show or hide the system UI.
        contentView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (TOGGLE_ON_CLICK) {
                    mSystemUiHider.toggle();
                } else {
                    mSystemUiHider.show();
                }
            }
        });

        DIR_INTERNAL = this.getDir(DIR, MODE_PRIVATE);
        try {
            md = MessageDigest.getInstance(ENCRYPT_ALGO);
        } catch (NoSuchAlgorithmException e) {
            // do nothing
        }
        dummyButton.setOnClickListener(checkListener);
        firstText.setOnEditorActionListener(firstTextListener);
        secondText.setOnEditorActionListener(secondTextListener);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

        // Trigger the initial hide() shortly after the activity has been
        // created, to briefly hint to the user that UI controls
        // are available.
        delayedHide(100);
    }

    @Override
    public void onBackPressed() {
        handleBack(this);
    }

    private static boolean exit;

    public static void handleBack(Activity activity) {
        if (exit) {
            activity.finish();
            exit = false;
        } else {
            exit = true;
            HANDLER.removeCallbacks(RESET_EXIT);
            HANDLER.postDelayed(RESET_EXIT, 3000);
        }
    }

    private static final Runnable RESET_EXIT = new Runnable() {
        @Override
        public void run() {
            exit = false;
        }
    };

    private File fileInternal, fileExternal;

    private final View.OnClickListener checkListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            checkPassword();
        }
    };

    private final View.OnClickListener confirmListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            confirmPassword();
        }
    };

    private final TextView.OnEditorActionListener firstTextListener = new TextView.OnEditorActionListener() {
        @Override
        public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
            boolean handled = false;
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                checkPassword();
                handled = true;
            }
            return handled;
        }
    };

    private final TextView.OnEditorActionListener secondTextListener = new TextView.OnEditorActionListener() {
        @Override
        public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
            boolean handled = false;
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                confirmPassword();
                handled = true;
            }
            return handled;
        }
    };

    private void checkPassword() {
        final String input = firstText.getText().toString();
        if (!input.isEmpty()) {
            if (!DIR_EXTERNAL.exists())
                DIR_EXTERNAL.mkdir();
            fileInternal = new File(DIR_INTERNAL, FILE);
            fileExternal = new File(DIR_EXTERNAL, FILE);
            if (fileInternal.exists()) {
                final byte[] digestedInput = md.digest(input.getBytes());
                final byte[] fileContents = readFileContents(fileInternal, R.string.error_io_read_password);
                if (!fileExternal.exists())
                    writeFileContents(fileExternal, fileContents, R.string.error_io_read_password);
                if (Arrays.equals(digestedInput, fileContents)) {
                    final Intent intent = new Intent(this, MainActivity2.class);
                    intent.putExtra(MainActivity2.KEY, input);
                    startActivity(intent);
                    HANDLER.postDelayed(clearText, 250);
                } else
                    showMessage(this, R.string.wrong_password);
            } else {
                if (fileExternal.exists()) {
                    writeFileContents(
                            fileInternal,
                            readFileContents(fileExternal, R.string.error_io_read_password),
                            R.string.error_io_read_password
                    );
                    checkPassword();
                } else {
                    secondText.setVisibility(View.VISIBLE);
                    secondText.requestFocus();
                    dummyButton.setOnClickListener(confirmListener);
                }
            }
        } else
            showMessage(this, R.string.error_empty);
    }

    private byte[] readFileContents(File file, int messageId) {
        BufferedInputStream bis = null;
        final byte[] fileContents = new byte[BYTE_LENGTH];
        try {
            bis = new BufferedInputStream(new FileInputStream(file));
            bis.read(fileContents);
        } catch (IOException e) {
            showMessage(this, messageId);
        } finally {
            try {
                if (bis != null)
                    bis.close();
            } catch (IOException e) {
                // do nothing
            }
        }
        return fileContents;
    }

    private void writeFileContents(File file, byte[] fileContents, int messageId) {
        BufferedOutputStream bos = null;
        try {
            bos = new BufferedOutputStream(
                    new FileOutputStream(file)
            );
            bos.write(fileContents);
            bos.flush();
        } catch (IOException|NullPointerException e) {
            showMessage(this, messageId);
        } finally {
            try {
                if (bos != null)
                    bos.close();
            } catch (IOException e) {
                // do nothing
            }
        }
    }

    private final Runnable clearText = new Runnable() {
        @Override
        public void run() {
            firstText.setText("");
        }
    };

    private void confirmPassword() {
        final String firstInput = firstText.getText().toString();
        final String secondInput = secondText.getText().toString();
        if (!secondInput.isEmpty()) {
            if (firstInput.equals(secondInput)) {
                if (md != null) {
                    final byte[] out = md.digest(secondInput.getBytes());
                    writeFileContents(fileInternal, out, R.string.error_io_create_password);
                    writeFileContents(fileExternal, out, R.string.error_io_create_password);
                    showMessage(this, R.string.password_created);
                    secondText.setVisibility(View.GONE);
                    secondText.setText("");
                    dummyButton.setOnClickListener(checkListener);
                    firstText.setText("");
                } else
                    showMessage(this, R.string.error_encrypt_password);
            } else {
                showMessage(this, R.string.error_confirm);
            }
        } else
            showMessage(this, R.string.error_empty);
    }

    private static Toast currToast;

    public static void showMessage(Activity activity, int strId) {
        if (currToast != null)
            currToast.cancel();
        currToast = Toast.makeText(activity, strId, Toast.LENGTH_SHORT);
        currToast.show();
    }

    private static final Handler HANDLER = new Handler();
    private final Runnable mHideRunnable = new Runnable() {
        @Override
        public void run() {
            mSystemUiHider.hide();
        }
    };

    /**
     * Schedules a call to hide() in [delay] milliseconds, canceling any
     * previously scheduled calls.
     */
    private void delayedHide(int delayMillis) {
        HANDLER.removeCallbacks(mHideRunnable);
        HANDLER.postDelayed(mHideRunnable, delayMillis);
    }
}
