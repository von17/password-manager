package com.app.root.app.items;

import java.util.Comparator;

public abstract class Item {

    static final Comparator<Item> ALPHABETICAL_COMPARATOR = new Comparator<Item>() {
        @Override
        public int compare(Item lhs, Item rhs) {
            return lhs.getTitle().compareToIgnoreCase(rhs.getTitle());
        }
    };
    static final Comparator<Item> DATE_MODIFIED_COMPARATOR = new Comparator<Item>() {
        @Override
        public int compare(Item lhs, Item rhs) {
            if (lhs.timeStamp == rhs.timeStamp)
                return 0;
            if (lhs.timeStamp > rhs.timeStamp)
                return 1;
            return -1;
        }
    };

    static Comparator<Item> currComparator = ALPHABETICAL_COMPARATOR;

    public long timeStamp;

    abstract String getTitle();

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof ParentItem) && !(o instanceof ChildItem))
            return false;
        return getTitle().equals(((Item) o).getTitle());
    }
}