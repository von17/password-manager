package com.app.root.app.items;

public class ChildItem extends Item {

    private static final String UNSPECIFIED = "No username";

    String user;
    byte[] password;

    ChildItem(String user, long timeStamp, byte[] password) {
        if (user.isEmpty())
            this.user = UNSPECIFIED;
        else
            this.user = user;
        this.timeStamp = timeStamp;
        this.password = password;
    }

    @Override
    String getTitle() {
        return user;
    }
}