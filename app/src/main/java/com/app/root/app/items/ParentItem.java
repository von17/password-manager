package com.app.root.app.items;

import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public class ParentItem extends Item {

    private static final int IV_POS = 0, USER_OFFSET = 0;
    private static final int HOST_POS = 1, TIME_STAMP_OFFSET = 1;
    private static final int TIME_STAMP_POS = 2, PASSWORD_OFFSET = 2;
    private static final int NUM_OF_CHILD_FIELDS = 3;

    public static final int ALPHABETICAL = 0;
    public static final int ALPHABETICAL_REVERSED = 1;
    public static final int DATE_MODIFIED = 2;
    public static final int DATE_MODIFIED_EARLIEST = 3;
    private static int currSortMode = 0;
    public static final int POS = 0;
    public static final int INSERT_POS = 1;

    public String filename;
    private byte[] iv;

    public String host;
    final List<Item> childItems;

    public ParentItem(String host, byte[] iv) {
        this.host = host;
        this.iv = iv;
        childItems = new ArrayList<>();
    }

    public boolean addChildItem(String user, String password, long timeStamp, byte[] key) throws IllegalBlockSizeException,
            InvalidKeyException, BadPaddingException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidAlgorithmParameterException {
        final ChildItem childItem = new ChildItem(user, timeStamp, null);
        final int[] result = getPosAndInsertPos(childItems, childItem);
        if (result[POS] > -1) {
            if (password.equals(
                    decrypt(key, ((ChildItem) (childItems.get(result[POS]))).password, iv)
            ))
                return false;
        } else
            result[INSERT_POS] = result[INSERT_POS] * -1 - 1;
        childItem.password = encrypt(key, password);
        childItems.add(result[INSERT_POS], childItem);
        this.timeStamp = timeStamp;
        return true;
    }

    public void addChildItem(ChildItem childItem) {
        final int pos = Collections.binarySearch(childItems, childItem, currComparator);
        if (pos > -1)
            childItems.add(pos, childItem);
        else
            childItems.add(pos * -1 - 1, childItem);
    }

    public void mergeChildItems(List<ChildItem> items, byte[] otherIv, byte[] key) throws NoSuchPaddingException, InvalidAlgorithmParameterException,
            NoSuchAlgorithmException, IllegalBlockSizeException, BadPaddingException,InvalidKeyException {
        for (ChildItem item : items) {
            final int[] result = getPosAndInsertPos(childItems, item);
            final byte[] itemPassword = encrypt(
                    key,
                    decrypt(key, item.password, otherIv)
            );
            if (result[POS] > -1) {
                byte[] otherItemPassword = ((ChildItem) childItems.get(result[POS])).password;
                if (!Arrays.equals(itemPassword, otherItemPassword)) {
                    item.password = itemPassword;
                    childItems.add(result[POS] + 1, item);
                }
            } else {
                item.password = itemPassword;
                childItems.add(result[INSERT_POS] * -1 - 1, item);
            }
        }
    }

    public ChildItem deleteChildItem(int childPosition) {
        return (ChildItem) (childItems.remove(childPosition));
    }

    public List<Byte[]> getEncryptedInstance(byte[] key) throws IllegalBlockSizeException, InvalidKeyException,
            BadPaddingException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidAlgorithmParameterException {
        final List<Byte[]> list = new ArrayList<>();
        list.add(byteArrayToObjectByteArray(iv));
        list.add(
                byteArrayToObjectByteArray(encrypt(key, host))
        );
        list.add(
                byteArrayToObjectByteArray(encrypt(key, Long.toString(timeStamp)))
        );
        for (Item item : childItems) {
            list.add(
                    byteArrayToObjectByteArray(encrypt(key, ((ChildItem) item).user))
            );
            list.add(
                    byteArrayToObjectByteArray(encrypt(key, Long.toString(timeStamp)))
            );
            list.add(byteArrayToObjectByteArray(((ChildItem) item).password));
        }
        return list;
    }

    private Byte[] byteArrayToObjectByteArray(byte[] bytes) {
        final Byte[] result = new Byte[bytes.length];
        for (int i = 0; i < bytes.length; i++)
            result[i] = bytes[i];
        return result;
    }

    private static byte[] byteArrayToPrimitiveByteArray(Byte[] bytes) {
        final byte[] result = new byte[bytes.length];
        for (int i = 0; i < bytes.length; i++)
            result[i] = bytes[i];
        return result;
    }

    private byte[] encrypt(byte[] key, String data) throws NoSuchPaddingException, NoSuchAlgorithmException,
            InvalidKeyException, BadPaddingException, IllegalBlockSizeException, InvalidAlgorithmParameterException {
        final Cipher c = Cipher.getInstance("AES/CBC/PKCS5PADDING");
        final SecretKeySpec k = new SecretKeySpec(key, "AES");
        c.init(Cipher.ENCRYPT_MODE, k, new IvParameterSpec(iv));
        return c.doFinal(data.getBytes());
    }

    public static ParentItem getDecryptedInstance(byte[] key, List<Byte[]> list, String filename) throws IllegalBlockSizeException, InvalidKeyException,
            BadPaddingException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidAlgorithmParameterException, NumberFormatException {
        final byte[] iv = byteArrayToPrimitiveByteArray(list.get(IV_POS));
        final ParentItem parentItem = new ParentItem(
                decrypt(key, byteArrayToPrimitiveByteArray(list.get(HOST_POS)), iv),
                iv
        );
        parentItem.timeStamp = Long.parseLong(decrypt(key, byteArrayToPrimitiveByteArray(list.get(TIME_STAMP_POS)), iv));
        parentItem.filename = filename;
        for (int i = TIME_STAMP_POS + 1; i < list.size(); i = i + NUM_OF_CHILD_FIELDS)
            parentItem.childItems.add(
                    new ChildItem(decrypt(key, byteArrayToPrimitiveByteArray(list.get(i + USER_OFFSET)), iv),
                            Long.parseLong(decrypt(key, byteArrayToPrimitiveByteArray(list.get(i + TIME_STAMP_OFFSET)), iv)),
                            byteArrayToPrimitiveByteArray(list.get(i + PASSWORD_OFFSET)))
            );
        return parentItem;
    }

    private static String decrypt(byte[] key, byte[] data, byte[] iv) throws NoSuchPaddingException, NoSuchAlgorithmException,
            InvalidKeyException, BadPaddingException, IllegalBlockSizeException, InvalidAlgorithmParameterException {
        final Cipher c = Cipher.getInstance("AES/CBC/PKCS5PADDING");
        final SecretKeySpec k = new SecretKeySpec(key, "AES");
        c.init(Cipher.DECRYPT_MODE, k, new IvParameterSpec(iv));
        return new String(c.doFinal(data));
    }

    public int childListSize() {
        return childItems.size();
    }

    public ChildItem getChild(int position) {
        return (ChildItem) (childItems.get(position));
    }

    public List<ChildItem> getChildItems() {
        final List<ChildItem> list = new ArrayList<>(childItems.size());
        for (Item item : childItems) {
            list.add((ChildItem) item);
        }
        return list;
    }

    public String getChildUser(int position) {
        return ((ChildItem) childItems.get(position)).user;
    }

    public void setChildUser(int position, String newUser) {
        ((ChildItem) childItems.get(position)).user = newUser;
    }

    public String getChildPassword(int position, byte[] key) throws NoSuchPaddingException, InvalidAlgorithmParameterException,
            NoSuchAlgorithmException, IllegalBlockSizeException, BadPaddingException, InvalidKeyException {
        return decrypt(key, ((ChildItem) childItems.get(position)).password, iv);
    }

    public byte[] getIv() {
        return iv;
    }

    public void sortChildItems() {
        Collections.sort(childItems, currComparator);
    }

    public static Comparator<Item> getCurrComparator() {
        return currComparator;
    }

    public static int getCurrSortMode() {
        return currSortMode;
    }

    public static void setSortMode(int sortMode) {
        switch (sortMode) {
            case DATE_MODIFIED_EARLIEST:
                currComparator = DATE_MODIFIED_COMPARATOR;
                currSortMode = DATE_MODIFIED_EARLIEST;
                break;
            case DATE_MODIFIED:
                currComparator = Collections.reverseOrder(DATE_MODIFIED_COMPARATOR);
                currSortMode = DATE_MODIFIED;
                break;
            case ALPHABETICAL_REVERSED:
                currComparator = Collections.reverseOrder(ALPHABETICAL_COMPARATOR);
                currSortMode = ALPHABETICAL_REVERSED;
                break;
            case ALPHABETICAL:
            default:
                currComparator = ALPHABETICAL_COMPARATOR;
                currSortMode = ALPHABETICAL;
        }
    }

    public static int[] getPosAndInsertPos(List<Item> items, Item item) {
        final int[] result = new int[2];
        if (currSortMode == DATE_MODIFIED || currSortMode == DATE_MODIFIED_EARLIEST) {
            result[POS] = items.indexOf(item);
            result[INSERT_POS] = Collections.binarySearch(items, item, currComparator);
        } else
            result[POS] = result[INSERT_POS] = Collections.binarySearch(items, item, currComparator);
        return result;
    }

    @Override
    String getTitle() {
        return host;
    }
}