package com.app.root.app;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseExpandableListAdapter;
import android.widget.CheckedTextView;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.app.root.app.items.ChildItem;
import com.app.root.app.items.Item;
import com.app.root.app.items.ParentItem;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

// TEst
public class MainActivity2 extends ActionBarActivity implements AdapterView.OnItemLongClickListener {

    public final static String KEY = "com.app.root.app.KEY";

    private final static String TAG_ADD_OR_EDIT_DIALOG = "com.app.root.app.ADD_OR_EDIT_DIALOG";
    private static final String GROUP_POSITION_KEY = "com.app.root.app.GROUP.POSITION.KEY";
    private static final String CHILD_POSITION_KEY = "com.app.root.app.CHILD.POSITION.KEY";
    private static final int NO_CHILD_POSITION = -1;
    private final static SortDialog SORT_DIALOG = new SortDialog();
    private final static String TAG_SORT_DIALOG = "com.app.root.app.SORT_DIALOG";
    private static final String ENCRYPT_ALGO = "SHA-256";
    private static final int BYTE_LENGTH = 16;
    private static final String SUB_DIR = "apps";
    private final static File SUB_DIR_INTERNAL = new File(MainActivity.DIR_INTERNAL, SUB_DIR);
    private final static File SUB_DIR_EXTERNAL = new File(MainActivity.DIR_EXTERNAL, SUB_DIR);
    private final static int RESULT_INTERNAL = 0;
    private final static int RESULT_EXTERNAL = 1;
    private final static List<Item> ITEMS = new ArrayList<>();
    private static ExpandableAdapter adapter;
    private static int maxFileNum;
    private static byte[] key;

    @InjectView(R.id.listview)
    ExpandableListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        ButterKnife.inject(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        try {
            final MessageDigest md = MessageDigest.getInstance(ENCRYPT_ALGO);
            key = md.digest(getIntent().getStringExtra(KEY).getBytes());
        } catch (NoSuchAlgorithmException e) {
            // do nothing
        }
        adapter = new ExpandableAdapter(this);
        listView.setAdapter(adapter);
        listView.setOnItemLongClickListener(this);

        if (!SUB_DIR_INTERNAL.exists()) {
            SUB_DIR_INTERNAL.mkdir();
            if (SUB_DIR_EXTERNAL.exists())
                readFiles(SUB_DIR_EXTERNAL.listFiles(), SUB_DIR_INTERNAL, this);
            else
                SUB_DIR_EXTERNAL.mkdir();
        } else {
            if (!SUB_DIR_EXTERNAL.exists())
                SUB_DIR_EXTERNAL.mkdir();
            final File[] filesInternal = SUB_DIR_INTERNAL.listFiles();
            final File[] filesExternal = SUB_DIR_EXTERNAL.listFiles();

            if (filesInternal.length >= filesExternal.length)
                readFiles(filesInternal, SUB_DIR_EXTERNAL, this);
            else
                readFiles(filesExternal, SUB_DIR_INTERNAL, this);
        }
    }

    private static final int NO_ERROR_MESSAGE = -2;

    private static void readFiles(File[] files, File otherDir, Activity activity) {
        for (final File file : files) {
            if (updateMaxFileNum(file.getName())) {
                final ParentItem parentItem = readParent(file, activity);
                final File otherFile = new File(otherDir, file.getName());
                if (!otherFile.exists())
                    writeParent(otherFile, parentItem, activity, NO_ERROR_MESSAGE, NO_ERROR_MESSAGE);
                ITEMS.add(parentItem);
            } else
                file.delete();
        }

        Collections.sort(ITEMS, ParentItem.getCurrComparator());
        adapter.notifyDataSetChanged();
    }

    private static boolean updateMaxFileNum(String name) {
        try {
            final int currIntFileName = Integer.parseInt(name);
            if (currIntFileName > maxFileNum)
                maxFileNum = currIntFileName;
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    private static boolean writeParent(File file, ParentItem parentItem, Activity activity, int errorIO, int errorEncrypt) {
        if (parentItem == null)
            return false;

        ObjectOutputStream oos = null;
        try {
            oos = new ObjectOutputStream(new FileOutputStream(file));
            oos.writeObject(parentItem.getEncryptedInstance(key));
        } catch (IOException e) {
            if (errorIO != NO_ERROR_MESSAGE)
                MainActivity.showMessage(activity, errorIO);
            return false;
        } catch (InvalidKeyException | BadPaddingException | NoSuchAlgorithmException |
                IllegalBlockSizeException | NoSuchPaddingException | InvalidAlgorithmParameterException e) {
            if (errorEncrypt != NO_ERROR_MESSAGE)
                MainActivity.showMessage(activity, errorEncrypt);
            return false;
        } finally {
            try {
                if (oos != null)
                    oos.close();
            } catch (IOException e) {
                // do nothing
            }
        }
        return true;
    }

    private static ParentItem readParent(File file, Activity activity) {
        ObjectInputStream ois = null;
        ParentItem parentItem = null;
        try {
            ois = new ObjectInputStream(new FileInputStream(file));
            parentItem = ParentItem.getDecryptedInstance(key, (List<Byte[]>)ois.readObject(), file.getName());
        } catch (IOException e) {
            MainActivity.showMessage(activity, R.string.error_io_read_item);
        } catch (ClassNotFoundException|InvalidKeyException|BadPaddingException|NoSuchAlgorithmException|
                IllegalBlockSizeException|NoSuchPaddingException|InvalidAlgorithmParameterException e) {
            file.delete();
            MainActivity.showMessage(activity, R.string.error_decrypt_read_item);
        } finally {
            try {
                if (ois != null)
                    ois.close();
            } catch (IOException e) {
                // do nothing
            }
        }
        return parentItem;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main_activity2, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case R.id.add:
                showDialog(new AddOrEditDialog(), TAG_ADD_OR_EDIT_DIALOG, getFragmentManager());
                return true;
            case R.id.sort:
                showDialog(SORT_DIALOG, TAG_SORT_DIALOG, getFragmentManager());
                return true;
            case android.R.id.home :
                MainActivity.handleBack(this);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private static void showDialog(DialogFragment dialog, String tag, FragmentManager fm) {
        final FragmentTransaction ft = fm.beginTransaction();
        final Fragment prev = fm.findFragmentByTag(tag);
        if (prev != null)
            ft.remove(prev);
        ft.addToBackStack(null);

        dialog.show(ft, tag);
    }

    @Override
    public void onBackPressed() {
        MainActivity.handleBack(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        ITEMS.clear();
    }

    public static class AddOrEditDialog extends DialogFragment {

        static EditText hostText, userText, passwordText;

        private static final String BUTTON_ID_KEY = "com.app.root.app.BUTTON.ID.KEY";
        private static final String LIST_KEY = "com.app.root.app.LIST.KEY";
        private static final String TAG_ADD_EXISTING_INFO_DIALOG = "com.app.root.app.ADD.EXISTING.INFO.DIALOG";

        private static List<String> hosts, users, passwords;

        private int groupPosition, childPosition;

        @InjectView(R.id.host_container)
        LinearLayout hostContainer;
        @InjectView(R.id.user_container)
        LinearLayout userContainer;
        @InjectView(R.id.password_container)
        LinearLayout passwordContainer;

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            final Bundle args = getArguments();
            groupPosition = args.getInt(GROUP_POSITION_KEY, -1);
            childPosition = args.getInt(CHILD_POSITION_KEY, NO_CHILD_POSITION);
            final View view = LayoutInflater.from(getActivity())
                    .inflate(R.layout.fragment_add_dialog, null);
            hostText = (EditText) view.findViewById(R.id.host_edit_text);
            userText = (EditText) view.findViewById(R.id.user_edit_text);
            passwordText = (EditText) view.findViewById(R.id.password_edit_text);
            ButterKnife.inject(this, view);

            final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setView(view)
                    .setNegativeButton(R.string.cancel, null);
            if (groupPosition == -1) {
                builder.setPositiveButton(R.string.add, addListener);
            } else {
                ParentItem parent = (ParentItem) ITEMS.get(groupPosition);
                if (childPosition > NO_CHILD_POSITION) {
                    hostContainer.setVisibility(View.GONE);
                    userText.setText(parent.getChildUser(childPosition));
                    try {
                        passwordText.setText(parent.getChildPassword(childPosition, key));
                    } catch (InvalidKeyException | BadPaddingException | NoSuchAlgorithmException |
                            IllegalBlockSizeException | NoSuchPaddingException | InvalidAlgorithmParameterException e) {
                        passwordText.setText(R.string.error_decrypt_password);
                    }
                } else {
                    userContainer.setVisibility(View.GONE);
                    passwordContainer.setVisibility(View.GONE);
                    hostText.setText(parent.host);
                }
                builder.setPositiveButton(R.string.edit, editListener);
            }
            return builder.create();
        }

        private final DialogInterface.OnClickListener editListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int which) {
                ParentItem parent = (ParentItem) ITEMS.get(groupPosition);
                if (childPosition > NO_CHILD_POSITION) {
                    final int sortMode = ParentItem.getCurrSortMode();
                    final String userInput = userText.getText().toString();
                    final String passwordInput = passwordText.getText().toString();
                    long time = -1;
                    if (!userInput.isEmpty() && !userInput.equals(parent.getChildUser(childPosition))) {
                        parent.setChildUser(childPosition, userInput);
                        time = System.currentTimeMillis();
                        if (sortMode == ParentItem.ALPHABETICAL || sortMode == ParentItem.ALPHABETICAL_REVERSED) {
                            Collections.sort(ITEMS, ParentItem.getCurrComparator());
                        }
                    }
                    // TODO: password and timestamp
                } else {
                    final String hostInput = hostText.getText().toString();
                    if (!hostInput.isEmpty() && !hostInput.equals(parent.host)) {
                        ITEMS.remove(parent);
                        if (!delFile(parent.filename)) {
                            MainActivity.showMessage(getActivity(), R.string.error_delete_host);
                        }
                        parent.host = hostInput;
                        final int[] result = ParentItem.getPosAndInsertPos(ITEMS, parent);
                        final int pos = result[ParentItem.POS];
                        final int insertPos = result[ParentItem.INSERT_POS];
                        parent.timeStamp = System.currentTimeMillis();
                        if (pos > -1) {
                            try {
                                final List<ChildItem> oldParentChildItems = parent.getChildItems();
                                final byte[] oldParentIv = parent.getIv();
                                parent = (ParentItem) ITEMS.get(pos);
                                parent.mergeChildItems(oldParentChildItems, oldParentIv, key);
                            } catch (InvalidKeyException | BadPaddingException | NoSuchAlgorithmException |
                                    IllegalBlockSizeException | NoSuchPaddingException | InvalidAlgorithmParameterException e) {
                                MainActivity.showMessage(getActivity(), R.string.error_decrypt_password);
                                MainActivity.showMessage(getActivity(), R.string.error_encrypt_password);
                            }
                        } else {
                            ITEMS.add(insertPos * -1 - 1, parent);
                        }
                        writeFile(parent, getActivity());
                    } else {
                        MainActivity.showMessage(getActivity(), R.string.not_edited);
                    }
                }
            }
        };

        private final DialogInterface.OnClickListener addListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                final String hostInput = hostText.getText().toString();
                final String passwordInput = passwordText.getText().toString();
                if (!hostInput.isEmpty() && !passwordInput.isEmpty()) {
                    final String userInput = userText.getText().toString();
                    final byte[] iv = new byte[BYTE_LENGTH];
                    new SecureRandom().nextBytes(iv);
                    ParentItem parentItem = new ParentItem(hostInput, iv);
                    final int[] result = ParentItem.getPosAndInsertPos(ITEMS, parentItem);
                    final int pos = result[ParentItem.POS];
                    final int insertPos = result[ParentItem.INSERT_POS];
                    if (pos > -1)
                        parentItem = (ParentItem) ITEMS.get(pos);
                    else
                        ITEMS.add(insertPos * -1 - 1, parentItem);
                    if (!passwordInput.isEmpty())
                        addUserPassword(parentItem, userInput, passwordInput);
                    else
                        MainActivity.showMessage(getActivity(), R.string.empty_password);
                } else
                    MainActivity.showMessage(getActivity(), R.string.required_user_password);
            }
        };

        private void addUserPassword(ParentItem parentItem, String userInput, String passwordInput) {
            try {
                if (parentItem.addChildItem(userInput, passwordInput, System.currentTimeMillis(), key)) {
                    if (parentItem.filename == null)
                        parentItem.filename = Integer.toString(++maxFileNum);

                    writeFile(parentItem, getActivity());

                    if (ParentItem.getCurrSortMode() == ParentItem.DATE_MODIFIED ||
                            ParentItem.getCurrSortMode() == ParentItem.DATE_MODIFIED_EARLIEST)
                        Collections.sort(ITEMS, ParentItem.getCurrComparator());
                    adapter.notifyDataSetChanged();
                } else
                    MainActivity.showMessage(getActivity(), R.string.exist_user_password);
            } catch (InvalidKeyException | BadPaddingException | NoSuchAlgorithmException |
                    IllegalBlockSizeException | NoSuchPaddingException | InvalidAlgorithmParameterException e) {
                MainActivity.showMessage(getActivity(), R.string.error_encrypt_create_item);
            }
        }

        @Override
        public void onDestroyView() {
            super.onDestroyView();
            ButterKnife.reset(this);
        }

        @OnClick({R.id.add_host_button, R.id.add_user_button, R.id.add_password_button})
        public void addExistingInfo(View view) {
            final Activity activity = getActivity();
            final int id = view.getId();
            switch (id) {
                case R.id.add_host_button:
                    addHosts();
                    showDialog(id, hosts, activity);
                    break;
                case R.id.add_user_button:
                    addUsers();
                    showDialog(id, users, activity);
                    break;
                case R.id.add_password_button:
                    addPasswords(activity);
                    showDialog(id, passwords, activity);
            }
        }

        private static void showDialog(int id, List<String> list, Activity activity) {
            final Bundle args = new Bundle();
            args.putInt(BUTTON_ID_KEY, id);
            final String[] array = new String[list.size()];
            args.putStringArray(LIST_KEY, list.toArray(array));

            final AddExistingInfoDialog dialog = new AddExistingInfoDialog();
            dialog.setArguments(args);
            MainActivity2.showDialog(dialog, TAG_ADD_EXISTING_INFO_DIALOG, activity.getFragmentManager());
        }

        private static void addHosts() {
            if (hosts == null)
                hosts = new ArrayList<>();

            for (Item item : ITEMS)
                insert(hosts, ((ParentItem) item).host);
        }

        private static void addUsers() {
            if (users == null)
                users = new ArrayList<>();

            for (Item item : ITEMS) {
                for (int i = 0; i < ((ParentItem) item).childListSize(); i++)
                    insert(users, ((ParentItem) item).getChildUser(i));
            }
        }

        private static void addPasswords(Activity activity) {
            if (passwords == null)
                passwords = new ArrayList<>();

            try {
                for (Item item : ITEMS) {
                    for (int i = 0; i < ((ParentItem) item).childListSize(); i++)
                        insert(passwords, ((ParentItem) item).getChildPassword(i, key));
                }
            } catch (InvalidKeyException | BadPaddingException | NoSuchAlgorithmException |
                    IllegalBlockSizeException | NoSuchPaddingException | InvalidAlgorithmParameterException e) {
                MainActivity.showMessage(activity, R.string.error_decrypt_password);
                passwords = null;
            }
        }

        private static void insert(List<String> list, String string) {
            final int pos = Collections.binarySearch(list, string, String.CASE_INSENSITIVE_ORDER);
            if (pos < 0)
                list.add(pos * -1 - 1, string);
        }

        public static class AddExistingInfoDialog extends DialogFragment {

            @Override
            public Dialog onCreateDialog(Bundle savedInstanceState) {
                final Bundle args = getArguments();
                final Dialog dialog = new AlertDialog.Builder(getActivity())
                        .setItems(args.getStringArray(LIST_KEY),
                                getListener(args.getInt(BUTTON_ID_KEY)))
                        .create();
                dialog.setCanceledOnTouchOutside(true);
                return dialog;
            }

            private static DialogInterface.OnClickListener getListener(int id) {
                switch (id) {
                    case R.id.add_host_button:
                        return new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                hostText.setText(hosts.get(which));
                            }
                        };
                    case R.id.add_user_button:
                        return new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                userText.setText(users.get(which));
                            }
                        };
                    case R.id.add_password_button:
                        return new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                passwordText.setText(passwords.get(which));
                                passwords.clear();
                            }
                        };
                    default:
                        return null;
                }
            }
        }
    }

    public static class SortDialog extends DialogFragment {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            final Dialog dialog = new AlertDialog.Builder(getActivity())
                    .setTitle(R.string.sort_by)
                    .setItems(R.array.sort_options, sortListener)
                    .create();
            dialog.setCanceledOnTouchOutside(true);
            return dialog;
        }

        private final DialogInterface.OnClickListener sortListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                ParentItem.setSortMode(which);
                Collections.sort(ITEMS, ParentItem.getCurrComparator());
                for (Item item : ITEMS)
                    ((ParentItem) item).sortChildItems();
                adapter.notifyDataSetChanged();
            }
        };
    }

    private static final String PASSWORD_KEY = "com.app.root.app.PASSWORD.KEY";
    private static final String TAG_PASSWORD_DIALOG = "com.app.root.app.PASSWORD.DIALOG";

    static class ExpandableAdapter extends BaseExpandableListAdapter {

        private final ActionBarActivity activity;

        ExpandableAdapter(ActionBarActivity activity) {
            this.activity = activity;
        }

        @Override
        public int getGroupCount() {
            return ITEMS.size();
        }

        @Override
        public int getChildrenCount(int groupPosition) {
            return ((ParentItem) ITEMS.get(groupPosition)).childListSize();
        }

        @Override
        public Object getGroup(int groupPosition) {
            return ITEMS.get(groupPosition);
        }

        @Override
        public Object getChild(int groupPosition, int childPosition) {
            return ((ParentItem) ITEMS.get(groupPosition)).getChild(childPosition);
        }

        @Override
        public long getGroupId(int groupPosition) {
            return groupPosition;
        }

        @Override
        public long getChildId(int groupPosition, int childPosition) {
            return getGroupCount() + childPosition;
        }

        @Override
        public boolean hasStableIds() {
            return false;
        }

        @Override
        public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
            ParentViewHolder holder;
            if (convertView == null) {
                convertView = LayoutInflater
                        .from(parent.getContext())
                        .inflate(R.layout.parent_view, parent, false);
                holder = new ParentViewHolder(convertView);
                convertView.setTag(holder);
            } else
                holder = (ParentViewHolder) convertView.getTag();

            holder.hostTextView.setChecked(isExpanded);
            holder.hostTextView.setText(
                    ((ParentItem) ITEMS.get(groupPosition)).host
            );
            return convertView;
        }

        @Override
        public View getChildView(final int groupPosition, final int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
            ChildViewHolder holder;
            if (convertView == null) {
                convertView = LayoutInflater
                        .from(parent.getContext())
                        .inflate(R.layout.child_view, parent, false);
                holder = new ChildViewHolder(convertView);
                convertView.setTag(holder);
            } else
                holder = (ChildViewHolder) convertView.getTag();

            holder.userTextView.setText(
                    ((ParentItem) ITEMS.get(groupPosition)).getChildUser(childPosition)
            );

            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        final Bundle args = new Bundle();
                        args.putString(
                                PASSWORD_KEY,
                                ((ParentItem) ITEMS.get(groupPosition)).getChildPassword(childPosition, key)
                        );
                        final PasswordDialog passwordDialog = new PasswordDialog();
                        passwordDialog.setArguments(args);
                        showDialog(passwordDialog, TAG_PASSWORD_DIALOG, activity.getFragmentManager());
                    } catch (InvalidKeyException | BadPaddingException | NoSuchAlgorithmException |
                            IllegalBlockSizeException | NoSuchPaddingException | InvalidAlgorithmParameterException e) {
                        MainActivity.showMessage(activity, R.string.error_decrypt_password);
                    }
                }
            });

            convertView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    activity.startSupportActionMode(new ItemActionModeCallback(groupPosition, childPosition, activity));
                    return true;
                }
            });
            return convertView;
        }

        @Override
        public boolean isChildSelectable(int groupPosition, int childPosition) {
            return true;
        }

        static class ParentViewHolder {

            @InjectView(R.id.host_text_view)
            CheckedTextView hostTextView;

            ParentViewHolder(View v) {
                ButterKnife.inject(this, v);
            }
        }

        static class ChildViewHolder {

            @InjectView(R.id.user_text_view)
            TextView userTextView;

            ChildViewHolder(View v) {
                ButterKnife.inject(this, v);
            }
        }
    }

    public static class PasswordDialog extends DialogFragment {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            final Bundle args = getArguments();
            final Dialog dialog = new AlertDialog.Builder(getActivity())
                    .setMessage(args.getString(PASSWORD_KEY))
                    .create();
            dialog.setCanceledOnTouchOutside(true);
            return dialog;
        }
    }

    @Override
    public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
        this.startSupportActionMode(new ItemActionModeCallback(position, this));
        return true;
    }

    private static final String TAG_DELETE_DIALOG = "com.app.root.app.DELETE.DIALOG";

    private static class ItemActionModeCallback implements ActionMode.Callback {

        private final int groupPosition;
        private final int childPosition;
        private final Activity activity;

        ItemActionModeCallback(int groupPosition, Activity activity) {
            this.groupPosition = groupPosition;
            this.childPosition = NO_CHILD_POSITION;
            this.activity = activity;
        }

        ItemActionModeCallback(int groupPosition, int childPosition, Activity activity) {
            this.groupPosition = groupPosition;
            this.childPosition = childPosition;
            this.activity = activity;
        }

        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            mode.getMenuInflater().inflate(R.menu.menu_context, menu);
            return true;
        }

        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            return false;
        }

        @Override
        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
            final Bundle args = new Bundle();
            args.putInt(GROUP_POSITION_KEY, groupPosition);
            if (childPosition != NO_CHILD_POSITION)
                args.putInt(CHILD_POSITION_KEY, childPosition);

            switch (item.getItemId()) {
                case R.id.edit_item:
                    final AddOrEditDialog editDialog = new AddOrEditDialog();
                    editDialog.setArguments(args);
                    showDialog(editDialog, TAG_ADD_OR_EDIT_DIALOG, activity.getFragmentManager());

                    mode.finish();
                    return true;
                case R.id.delete_item:
                    final DeleteDialog deleteDialog = new DeleteDialog();
                    deleteDialog.setArguments(args);
                    showDialog(deleteDialog, TAG_DELETE_DIALOG, activity.getFragmentManager());

                    mode.finish();
                    return true;
                default:
                    return false;
            }
        }

        @Override
        public void onDestroyActionMode(ActionMode mode) {
            // do nothing
        }
    }

    public static class DeleteDialog extends DialogFragment {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            final Bundle args = getArguments();
            final int groupPosition = args.getInt(GROUP_POSITION_KEY);
            final int childPosition = args.getInt(CHILD_POSITION_KEY, NO_CHILD_POSITION);

            final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            if (childPosition == NO_CHILD_POSITION) {
                builder.setTitle(R.string.delete_host_title);
                builder.setMessage(R.string.delete_host);
            } else {
                builder.setTitle(R.string.delete_user_title);
                builder.setMessage(R.string.delete_user);
            }

            builder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    if (childPosition == NO_CHILD_POSITION)
                        deleteHost(groupPosition, getActivity());
                    else
                        deleteUser((ParentItem) ITEMS.get(groupPosition), childPosition, getActivity());
                }
            });
            builder.setNegativeButton(R.string.no, null);

            return builder.create();
        }
    }

    private static void deleteUser(ParentItem parentItem, int childPosition, Activity activity) {
        final ChildItem childItem = parentItem.deleteChildItem(childPosition);
        final boolean resultInternal = writeParent(
                new File(SUB_DIR_INTERNAL, parentItem.filename),
                parentItem,
                activity,
                R.string.error_delete_user,
                R.string.error_delete_user
        );
        final boolean resultExternal = writeParent(
                new File(SUB_DIR_EXTERNAL, parentItem.filename),
                parentItem,
                activity,
                R.string.error_delete_user,
                R.string.error_delete_user
        );

        if (!resultInternal && !resultExternal)
            parentItem.addChildItem(childItem);
        else
            adapter.notifyDataSetChanged();
    }

    private static void deleteHost(int groupPosition, Activity activity) {
        final ParentItem parentItem = (ParentItem) ITEMS.get(groupPosition);

        if (delFile(parentItem.filename)) {
            ITEMS.remove(groupPosition);
            adapter.notifyDataSetChanged();
        } else
            MainActivity.showMessage(activity, R.string.error_delete_host);
    }

    private static boolean delFile(String filename) {
        final boolean resultInternal = new File(SUB_DIR_INTERNAL, filename).delete();
        final boolean resultExternal = new File(SUB_DIR_EXTERNAL, filename).delete();

        return resultInternal && resultExternal;
    }

    private static void writeFile(ParentItem item, Activity activity) {
        writeParent(
                new File(SUB_DIR_INTERNAL, item.filename),
                item,
                activity,
                R.string.error_io_create_item,
                R.string.error_encrypt_create_item
        );
        writeParent(
                new File(SUB_DIR_EXTERNAL, item.filename),
                item,
                activity,
                R.string.error_io_create_item,
                R.string.error_encrypt_create_item
        );
    }
}
